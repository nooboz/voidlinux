syntax on
set shell=/bin/sh
set number
set hlsearch
set incsearch
set ruler
set mouse=a
"set mouse=v
set smartcase
set autoindent
set ignorecase
set tabstop=4
set shiftwidth=4
set expandtab
set softtabstop=4
set wildmenu
set laststatus=2
set wildmode=list:longest,full
" set list
set autoread
set cmdheight=1
set splitbelow splitright
set scrolljump=5
set scrolloff=2
set foldmethod=syntax
set t_Co=256
set textwidth=110
set history=9637
set hidden
set showmode
set encoding=utf-8
set linespace=0
set clipboard=unnamedplus
set nocompatible
filetype plugin indent on
"colorscheme delek
colorscheme hybrid
let $VIMROOT = expand("$HOME") . "/vimfiles"

"" Status-line
"set statusline=
"set statusline+=%#IncSearch#
"set statusline+=\ %y
"set statusline+=\ %r
"set statusline+=%#CursorLineNr#
"set statusline+=\ %F
"set statusline+=%= "Right side settings
"set statusline+=%#Search#
"set statusline+=\ %l/%L
"set statusline+=\ [%c]

nnoremap j gj
nnoremap k gk

vnoremap <C-c> "+y
map <C-c> "+P
vnoremap <C-c> "*y :let @+=@*<CR>

"" Status-line new
"function! GitBranch()
"  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
"endfunction
"
"function! StatuslineGit()
"  let l:branchname = GitBranch()
"  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
"endfunction

set statusline=
set statusline+=%#PmenuSel#
"set statusline+=%{StatuslineGit()}
set statusline+=%#LineNr#
set statusline+=\ %f
"set statusline+=%m\
set statusline+=%m
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\[%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
"set statusline+=\ [%{strftime('%Y/%b/%d\ %a\ %I:%M\ %p')}\]\ 
set statusline+=\ [%{strftime('%Y/%b/%d\ %a\ %I:%M\ %p')}\]
set statusline+=\ 
"
"colo habamax
colo delek
"
